package DTO;

import java.util.List;

public class TreeDTO {
    private String directoryName;

    private List<TreeDTO> subDirectories;

    public TreeDTO(String directoryName, List<TreeDTO> subDirectories) {
        this.directoryName = directoryName;
        this.subDirectories = subDirectories;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public List<TreeDTO> getSubDirectories() {
        return subDirectories;
    }

    public void setSubDirectories(List<TreeDTO> subDirectories) {
        this.subDirectories = subDirectories;
    }
}
