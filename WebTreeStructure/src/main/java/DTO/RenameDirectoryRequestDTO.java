package DTO;

public class RenameDirectoryRequestDTO {
    private String[] targetDirectoryPath;
    private String newName;

    public RenameDirectoryRequestDTO(String[] targetDirectoryPath, String newName) {
        this.targetDirectoryPath = targetDirectoryPath;
        this.newName = newName;
    }

    public String[] getTargetDirectoryPath() {
        return targetDirectoryPath;
    }

    public void setTargetDirectoryPath(String[] targetDirectoryPath) {
        this.targetDirectoryPath = targetDirectoryPath;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }
}
