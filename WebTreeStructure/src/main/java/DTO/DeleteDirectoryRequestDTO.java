package DTO;

public class DeleteDirectoryRequestDTO {
    private String[] targetDirectoryPath;

    public DeleteDirectoryRequestDTO(String[] targetDirectoryPath) {
        this.targetDirectoryPath = targetDirectoryPath;
    }

    public String[] getTargetDirectoryPath() {
        return targetDirectoryPath;
    }

    public void setTargetDirectoryPath(String[] targetDirectoryPath) {
        this.targetDirectoryPath = targetDirectoryPath;
    }
}
