package DTO;

public class MoveDirectoryRequestDTO {
    private String[] directoryToMovePath;
    private String[] newDirectoryPath;

    public MoveDirectoryRequestDTO(String[] directoryToMovePath, String[] newDirectoryPath) {
        this.directoryToMovePath = directoryToMovePath;
        this.newDirectoryPath = newDirectoryPath;
    }
    public String[] getDirectoryToMovePath() {
        return directoryToMovePath;
    }

    public void setDirectoryToMovePath(String[] directoryToMovePath) {
        this.directoryToMovePath = directoryToMovePath;
    }

    public String[] getNewDirectoryPath() {
        return newDirectoryPath;
    }

    public void setNewDirectoryPath(String[] newDirectoryPath) {
        this.newDirectoryPath = newDirectoryPath;
    }

}
