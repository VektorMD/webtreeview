package DTO;

public class AddDirectoryRequestDTO {
    private String newDirectoryName;
    private String[] targetDirectoryPath;

    public AddDirectoryRequestDTO(String newDirectoryName, String[] targetDirectoryPath) {
        this.newDirectoryName = newDirectoryName;
        this.targetDirectoryPath = targetDirectoryPath;
    }

    public String getNewDirectoryName() {
        return newDirectoryName;
    }

    public void setNewDirectoryName(String newDirectoryName) {
        this.newDirectoryName = newDirectoryName;
    }

    public String[] getTargetDirectoryPath() {
        return targetDirectoryPath;
    }

    public void setTargetDirectoryPath(String[] targetDirectoryPath) {
        this.targetDirectoryPath = targetDirectoryPath;
    }


}
