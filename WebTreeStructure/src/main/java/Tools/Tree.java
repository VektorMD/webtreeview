package Tools;

import java.util.ArrayList;
import java.util.List;

public class Tree {
    private String directoryName;
    private List<Tree> leafs;

    public Tree(String directoryName) {
        this.directoryName = directoryName;
        leafs = new ArrayList<Tree>();
    }

    public Tree(String directoryName, List<Tree> leafs) {
        this.directoryName = directoryName;
        this.leafs = leafs;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public List<Tree> getLeafs() {
        return leafs;
    }

    public void setLeafs(List<Tree> leafs) {
        this.leafs = leafs;
    }

    public void addLeaf(String[] path, String newDirectoryName) {
        Tree node = findNode(path);
        if (node == null) {
            return;
        }
        for (Tree tree: node.leafs) {
            if (tree.directoryName.equalsIgnoreCase(newDirectoryName)) {
                return;
            }
        }
        node.leafs.add(new Tree(newDirectoryName));
    }

    private Tree findNode(String[] path) {
        Tree node = this;
        if (!directoryName.equalsIgnoreCase(path[0])) {
            return null;
        }
        boolean flagFoundTarget = false;
        if (path.length == 1) {
            flagFoundTarget = true;
        }
        outer:
        for (int i = 1; i < path.length; i++) {
            for (Tree tree: node.leafs) {
                if (tree.directoryName.equalsIgnoreCase(path[i])) {
                    node = tree;
                    if (i == (path.length - 1)) {
                        flagFoundTarget = true;
                        break outer;
                    }
                    continue outer;
                }
            }
            // path doesn't exist
            break;
        }
        if (flagFoundTarget) {
            return node;
        } else {
            return null;
        }
    }

    public void deleteDirectory(String[] path) {
        String[] parentPath = new String[path.length - 1];
        System.arraycopy(path, 0, parentPath, 0, path.length - 1);
        String dirName = path[path.length - 1];
        Tree node = findNode(parentPath);
        for (Tree leaf: node.leafs) {
            if (leaf.directoryName.equalsIgnoreCase(dirName)) {
                node.leafs.remove(leaf);
                break;
            }
        }
    }

    public void moveDirectory(String[] oldPath, String[] targetPath) {
        String[] oldParentPath = new String[oldPath.length - 1];
        System.arraycopy(oldPath, 0, oldParentPath, 0, oldPath.length - 1);
        String dirName = oldPath[oldPath.length - 1];
        Tree nodeOldParent = findNode(oldParentPath);
        Tree nodeNewParent = findNode(targetPath);

        if (nodeOldParent == null || nodeNewParent == null) {
            return;
        }

        Tree targetNode = null;
        for (Tree leaf: nodeOldParent.leafs) {
            if (leaf.directoryName.equalsIgnoreCase(dirName)) {
                targetNode = leaf;
                nodeOldParent.leafs.remove(leaf);
                break;
            }
        }
        if (targetNode != null) {
            nodeNewParent.leafs.add(targetNode);
        }
    }

    public void renameDirectory(String[] path, String newName) {
        String[] parentPath = new String[path.length - 1];
        System.arraycopy(path, 0, parentPath, 0, path.length - 1);
        String dirName = path[path.length - 1];
        Tree node = findNode(parentPath);
        for (Tree leaf: node.leafs) {
            if (leaf.directoryName.equalsIgnoreCase(dirName)) {
                leaf.directoryName = newName;
            }
        }
    }
}
