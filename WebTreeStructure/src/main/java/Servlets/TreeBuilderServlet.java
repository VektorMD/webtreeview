package Servlets;

import DTO.*;
import Tools.Tree;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TreeBuilderServlet extends HttpServlet {
    private static Tree tree;
    static {
        tree = new Tree("RootDir");
        List<String> path = new ArrayList<String>();
        path.add("RootDir");
        tree.addLeaf(path.toArray(new String[path.size()]), "Sub1");
        tree.addLeaf(path.toArray(new String[path.size()]), "Sub2");
        tree.addLeaf(path.toArray(new String[path.size()]), "Sub3");
        path.add("Sub1");
        tree.addLeaf(path.toArray(new String[path.size()]), "SSub1");
        tree.addLeaf(path.toArray(new String[path.size()]), "SSub2");
        path.remove(path.size() - 1);
        path.add("Sub2");
        tree.addLeaf(path.toArray(new String[path.size()]), "SSub1");
        tree.addLeaf(path.toArray(new String[path.size()]), "SSub2");
        tree.addLeaf(path.toArray(new String[path.size()]), "SSub3");
        path.add("SSub3");
        tree.addLeaf(path.toArray(new String[path.size()]), "SSSub1");
        tree.addLeaf(path.toArray(new String[path.size()]), "SSSub2");
        path.remove(path.size() - 1);
        path.remove(path.size() - 1);
        tree.addLeaf(path.toArray(new String[path.size()]),"Sub3");
        tree.addLeaf(path.toArray(new String[path.size()]),"test");

    }
    public TreeBuilderServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        String json = new Gson().toJson(tree);
        response.getWriter().write(json);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        if (br == null) {
            return;
        }
        String json = br.readLine();

        RequestTypeDTO requestTypeDTO = new Gson().fromJson(json, RequestTypeDTO.class);
        System.out.println(requestTypeDTO.getRequestType());

        String requestType = requestTypeDTO.getRequestType();

//        String dirName = request.getParameter("directoryName");
//        String[] dirPath = request.getParameterValues("directoryPath");
//        List<String> lDirPath = new ArrayList<>(Arrays.asList(dirPath));
        switch (requestType) {
            case "AddDirectory": {
                AddDirectoryRequestDTO requestDTO = new Gson().fromJson(json, AddDirectoryRequestDTO.class);
                String[] path = requestDTO.getTargetDirectoryPath();
                if (path == null) {
                    tree = new Tree(requestDTO.getNewDirectoryName());
                } else {
                    tree.addLeaf(requestDTO.getTargetDirectoryPath(), requestDTO.getNewDirectoryName());
                }
            }
                break;
            case "DeleteDirectory": {
                DeleteDirectoryRequestDTO requestDTO = new Gson().fromJson(json, DeleteDirectoryRequestDTO.class);
                String[] path = requestDTO.getTargetDirectoryPath();
                if (path.length == 1 && path[0].equalsIgnoreCase(tree.getDirectoryName())) {
                    tree = null;
                } else {
                    tree.deleteDirectory(path);
                }
            }
                break;
            case "RenameDirectory": {
                RenameDirectoryRequestDTO requestDTO = new Gson().fromJson(json, RenameDirectoryRequestDTO.class);
                tree.renameDirectory(requestDTO.getTargetDirectoryPath(), requestDTO.getNewName());
            }
                break;
            case "MoveDirectory": {
                MoveDirectoryRequestDTO requestDTO = new Gson().fromJson(json, MoveDirectoryRequestDTO.class);
                tree.moveDirectory(requestDTO.getDirectoryToMovePath(), requestDTO.getNewDirectoryPath());
            }
                break;
            default: {

            }
        }
    }
}
