document.addEventListener("DOMContentLoaded", function () {
    loadTreeFromServer();

    var span = document.getElementById("closeModalCD");
    span.onclick = function (ev) {
        var modal = document.getElementById("modalCreateDirectory");
        modal.style.display = "none";
    }

    span = document.getElementById("closeModalCDD");
    span.onclick = function (ev) {
        var modal = document.getElementById("modalConfirmDeleteDirectory");
        modal.style.display = "none";
    }

    span = document.getElementById("closeModalRD");
    span.onclick = function (ev) {
        var modal = document.getElementById("modalRenameDirectory");
        modal.style.display = "none";
    }
});

function loadTreeFromServer() {
    var url = "TreeBuilderServlet";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = new Array(JSON.parse(this.responseText));
            var ulRoot;
            if (myArr[0] == null) {
                ulRoot = document.createElement("ul");
            } else {
                ulRoot = buildTreeViewFromArray(myArr);
            }
            ulRoot.classList.remove("nested");
            ulRoot.setAttribute("id", "root")
            var div = document.getElementById("tree");
            div.appendChild(ulRoot);
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
}

function buildTreeViewFromArray(arr) {
    var ul = document.createElement("ul");
    ul.classList.add("nested");
    for (var i = 0; i < arr.length; i++) {
        var li  = document.createElement("li");

        var div = document.createElement("div");
        li.appendChild(div);

        var span = document.createElement("span");
        span.textContent = arr[i].directoryName;
        span.addEventListener("click", spanClickListener);
        span.setAttribute("draggable", "true");
        span.addEventListener("dragstart", drag);
        span.addEventListener("dragover", allowDrop);
        span.addEventListener("drop", drop);
        div.appendChild(span);

        if (arr[i].leafs.length != 0) {
            var img = document.createElement("img");
            img.setAttribute("src", "images/rightTriangle.png");
            img.setAttribute("class", "containing");
            img.addEventListener("click", onExpandableClickListener);
            div.insertBefore(img, div.childNodes[0]);
            li.appendChild(buildTreeViewFromArray(arr[i].leafs));
        }
        ul.appendChild(li);
    }
    return ul;
}

// function createDirectoryLiNode(arrElement) {
//     var li = document.createElement("li");
//
//     var div = document.createElement("div");
//     li.appendChild(div);
//
//     var span = document.createElement("span");
//     span.textContent = arrElement.directoryName;
//     span.addEventListener("click", spanClickListener);
//     span.setAttribute("draggable", "true");
//     span.addEventListener("dragstart", drag);
//     span.addEventListener("dragover", allowDrop);
//     span.addEventListener("drop", drop);
//     div.appendChild(span);
//
//     if (arrElement.leafs.length != 0) {
//         var img = document.createElement("img");
//         img.setAttribute("src", "images/rightTriangle.png");
//         img.setAttribute("class", "containing");
//         img.addEventListener("click", onExpandableClickListener);
//         div.insertBefore(img, div.childNodes[0]);
//         li.appendChild(buildTreeViewFromArray(arrElement.leafs));
//     }
//     return li;
// }

function spanClickListener() {
    var curSelected = document.getElementById("selected");
    if (curSelected != null) {
        curSelected.removeAttribute("id");
    }
    this.setAttribute("id", "selected");
}

function onExpandableClickListener() {
    var t = this.parentElement.parentElement.querySelector(".nested");
    // Если не директория не раскрыта, добавляем индикацию загрузки
    if (!this.classList.contains("containing-expand")) {
        var ldng = document.createElement("img");
        ldng.setAttribute("src", "../images/loading.gif");
        ldng.classList.add("loadingimg");
        this.parentElement.appendChild(ldng);
    }
    this.classList.toggle("containing-expand");

    var func = function(toExpand) {
        if (!toExpand.classList.contains("active")) {
            var ldngImg = toExpand.parentElement.querySelector(".loadingimg");
            ldngImg.parentElement.removeChild(ldngImg);
        }
        var selected = document.getElementById("selected");
        if (selected != null && toExpand.contains(selected) && toExpand.classList.contains("active")) {
            selected.removeAttribute("id");
            toExpand.parentElement.querySelector("span").setAttribute("id", "selected");
        }
        toExpand.classList.toggle("active");
        // this.classList.toggle("containing-expand");
        var nodes = toExpand.children;
        for (var j = 0; j < nodes.length; j++) {
            var tmp = nodes[j].getElementsByClassName("active");
            for (var t = 0; t < tmp.length; t++) {
                tmp[t].classList.toggle("active");
            }
            tmp = nodes[j].getElementsByClassName("containing-expand");
            for (var t = 0; t < tmp.length; t++) {
                tmp[t].classList.toggle("containing-expand");
            }
        }
    };

    if (t.classList.contains("active")) {
        func(t)
    } else {
        setTimeout(func, 2000, t);
    }
}

function showNewDirectoryDialog() {
    var modal = document.getElementById("modalCreateDirectory");
    modal.style.display = "block";
}

function getTargetPath(spanTarget) {
    var res = new Array(spanTarget.textContent);
    var parentLi = spanTarget.parentElement.parentElement;
    // Ищем li папки-родителя родителя:
    var node = parentLi.parentElement;
    while (true) {
        if (node == null || node.tagName == "LI") {
            break;
        }
        node = node.parentNode;
    }
    if (node == null) {
        return res;
    } else {
        var arr = getTargetPath(node.querySelector("span"));
        return arr.concat(res);
    }
}

function createDirectory() {
    // li >> div >> span (selectable)
    // li >> div >> ul (nested -- subDirectory)
    var textField = document.getElementById("newDirName");
    var dirName = textField.value;
    if (dirName.length == 0) {
        return;
    }

    var li = document.createElement("li");
    var div = document.createElement("div");
    li.appendChild(div);
    var span = document.createElement("span");
    span.textContent = dirName;
    span.addEventListener("click", spanClickListener);
    span.setAttribute("draggable", "true");
    span.addEventListener("dragstart", drag);
    span.addEventListener("dragover", allowDrop);
    span.addEventListener("drop", drop);
    div.appendChild(span);

    var selected = document.getElementById("selected");
    if (selected != null) {

        var subDir = selected.parentElement.parentElement.querySelector(".nested");
        var img = selected.parentElement.querySelector(".containing");
        if (img == null) {
            img = document.createElement("img");
            img.setAttribute("src", "images/rightTriangle.png");
            img.setAttribute("class", "containing");
            img.addEventListener("click", onExpandableClickListener);
            var parDiv = selected.parentElement;
            parDiv.insertBefore(img, parDiv.childNodes[0]);
        }

        var url = "TreeBuilderServlet";
        var xhttp = new XMLHttpRequest();
        var params = JSON.stringify({requestType:"AddDirectory", targetDirectoryPath:getTargetPath(selected), newDirectoryName:dirName});
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(params);

        var root = document.getElementById("root");
        if (root.childNodes.length == 0) {
            root.appendChild(li);
        } else {
            if (subDir == null) {
                subDir = document.createElement("ul");
                subDir.classList.add("nested");
                subDir.appendChild(li);
                selected.parentElement.parentElement.appendChild(subDir)
            } else {
                for (var child in subDir.childNodes) {
                    if (subDir.childNodes[child].nodeType == 1) {
                        var selectableElement = subDir.childNodes[child].querySelector("span");
                        if (selectableElement != null && selectableElement.innerHTML.toLowerCase() == dirName.toLowerCase()) {
                            return;
                        }
                    }
                }
                subDir.appendChild(li);
            }
            if (!img.classList.contains("containing-expand")) {
                img.click();
            }
        }
        document.getElementById("closeModalCD").click();
        textField.value = "";
    } else {
        var root = document.getElementById("root");
        if (root.childNodes.length == 0) {
            root.appendChild(li);

            var url = "TreeBuilderServlet";
            var xhttp = new XMLHttpRequest();
            var params = JSON.stringify({requestType:"AddDirectory", targetDirectoryPath:null, newDirectoryName:dirName});
            xhttp.open("POST", url, true);
            xhttp.setRequestHeader("Content-type", "application/json");
            xhttp.send(params);
        }
        document.getElementById("closeModalCD").click();
        textField.value = "";
    }
}

function showDeleteDirectoryDialog() {
    var modal = document.getElementById("modalConfirmDeleteDirectory");
    var selected = document.getElementById("selected");
    var confirmMessage = document.getElementById("confirmDeleteMessage");
    var msg = "Do you really want to delete <b>" + selected.textContent + "</b> directory?";
    confirmMessage.innerHTML = msg;
    modal.style.display = "block";
}

function deleteDirectory() {
    var selected = document.getElementById("selected");
    // ul >> li >> div >> span (selected)
    var li = selected.parentElement.parentElement;
    var ul = li.parentElement;
    if (selected != null) {
        var url = "TreeBuilderServlet";
        var xhttp = new XMLHttpRequest();
        var params = JSON.stringify({requestType:"DeleteDirectory", targetDirectoryPath:getTargetPath(selected)});
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(params);

        if (ul.getElementsByTagName("li").length > 1) {
            ul.removeChild(li);
        } else {
            var ulPar = ul.parentElement;
            ulPar.removeChild(ul);
            var img = ulPar.querySelector(".containing");
            img.parentElement.removeChild(img);
        }
    }
    document.getElementById("closeModalCDD").click();
}

function showRenameDirectoryDialog() {
    var modal = document.getElementById("modalRenameDirectory");
    var selected = document.getElementById("selected");
    if (selected == null) {
        return;
    }
    var newNameTextField = document.getElementById("newNameTextField");
    newNameTextField.value = selected.textContent;
    modal.style.display = "block";
}

function renameDirectory() {
    var selected = document.getElementById("selected");
    // ul >> li >> div >> span (selected)
    // var dirName = selected.textContent;
    var newDirName = document.getElementById("newNameTextField").value;
    var li = selected.parentElement.parentElement;
    var ul = li.parentElement;
    if (selected != null) {
        for (var child in ul.childNodes) {
            if (ul.childNodes[child].nodeType == 1) {
                var selectableElement = ul.childNodes[child].querySelector("span");
                if (selectableElement != null && selectableElement.innerHTML.toLowerCase() == newDirName.toLowerCase()) {
                    return;
                }
            }
        }
        var url = "TreeBuilderServlet";
        var xhttp = new XMLHttpRequest();
        var params = JSON.stringify({requestType:"RenameDirectory", targetDirectoryPath:getTargetPath(selected), newName:newDirName});
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(params);

        selected.textContent = newDirName;
    }
    document.getElementById("closeModalRD").click();
}

function drag(dragevent) {
    dragevent.target.setAttribute("id", "dragged");
    // dragevent.dataTransfer.setData("text", dragevent.target.id);

}

function allowDrop(allowdropevent) {
    allowdropevent.preventDefault();
}

function drop(dropevent) {
    dropevent.preventDefault();
    // target is a span
    // li >> div >> span
    // если у этой папки есть подпапки, то li содержит ul с класслом nested
    var target = dropevent.target;
    var realTarget = target.parentElement.parentElement;
    var draggedElement = document.getElementById("dragged");
    draggedElement.removeAttribute("id");
    if (target == draggedElement) {
        return;
    }
    // draggedElement - это span, а нам нужен li:
    var elementToMove = draggedElement.parentElement.parentElement;
    // Место, откуда переносим элемент li:
    var ulFrom = elementToMove.parentElement;
    // Проверяем, что мы не пытаемся перенести папку в её же подпапку
    if (isDescendant(elementToMove, realTarget)) {
        return;
    }
    var dirName = elementToMove.textContent;
    var subDir = realTarget.querySelector(".nested");
    var img = target.parentElement.querySelector(".containing");
    if (subDir == null) {
        var img = document.createElement("img");
        img.setAttribute("src", "images/rightTriangle.png");
        img.setAttribute("class", "containing");
        img.addEventListener("click", onExpandableClickListener);
        var parDiv = target.parentElement;
        parDiv.insertBefore(img, parDiv.childNodes[0]);

        subDir = document.createElement("ul");
        subDir.classList.add("nested");
        subDir.appendChild(elementToMove);
        realTarget.appendChild(subDir);
    } else {
        for (var child in subDir.childNodes) {
            if (subDir.childNodes[child].nodeType === 1) {
                var selectableElement = subDir.childNodes[child].querySelector("span");
                if (selectableElement != null && selectableElement.innerHTML.toLowerCase() === dirName.toLowerCase()) {
                    return;
                }
            }
        }
        var url = "TreeBuilderServlet";
        var xhttp = new XMLHttpRequest();
        var params = JSON.stringify({requestType:"MoveDirectory",
            directoryToMovePath:getTargetPath(draggedElement),
            newDirectoryPath:getTargetPath(target)});
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(params);

        subDir.appendChild(elementToMove);
    }
    // if (!img.classList.contains("containing-expand")) {
    //     img.click();
    // }

    // Если папка, из которой перенесли теперь пуста, надо удалить индикацию
    if (ulFrom.getElementsByTagName("li").length === 0) {
        var ulPar = ulFrom.parentElement;
        ulPar.removeChild(ulFrom);
        var img = ulPar.querySelector(".containing");
        img.parentElement.removeChild(img);
    }
}

function isDescendant(parent, child) {
    var node = child.parentNode;
    while (node != null) {
        if (node == parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}