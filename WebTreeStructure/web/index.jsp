<%--
  Created by IntelliJ IDEA.
  User: inarb
  Date: 01.11.2018
  Time: 20:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
    <script type="text/javascript" src="scripts/allscripts.js"></script>
    <link type="text/css" rel="stylesheet" href="css/treepage.css">
  </head>
  <body>
  <div id="content">
    <div id="controlBar">
      <button class="btn" id="btnCreate" onclick="showNewDirectoryDialog()"><img src="images/add-folder-26.png"></button>
      <button class="btn" id="btnDelete" onclick="showDeleteDirectoryDialog()"><img src="images/delete-26.png"></button>
      <button class="btn" id="btnRename" onclick="showRenameDirectoryDialog()"><img src="images/rename-26.png"></button>
    </div>
    <div id="tree">
    </div>
  </div>

  <div id="modalCreateDirectory" class="modal">
    <div class="modal-content">
      <div class="modal-header">
        <span class="close" id="closeModalCD">×</span>
        <h2>Create folder</h2>
      </div>
      <div class="modal-body">
        <div class="modal-body-part">
          <div class="modal-message">Enter a name:</div>
        </div>
        <div class="modal-body-part">
          <input id="newDirName" type="text" class="inputText">
        </div>
        <div class="modal-body-part">
          <button class="btn" onclick="createDirectory()">Create</button>
        </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>

  <div id="modalConfirmDeleteDirectory" class="modal">
    <div class="modal-content">
      <div class="modal-header">
        <span class="close" id="closeModalCDD">×</span>
        <h2>Confirm delete action</h2>
      </div>
      <div class="modal-body">
        <div class="modal-body-part">
          <div class="modal-message" id="confirmDeleteMessage"></div>
        </div>
        <div class="modal-body-part">
          <button class="btn" onclick="deleteDirectory()">Confirm</button>
        </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>

  <div id="modalRenameDirectory" class="modal">
    <div class="modal-content">
      <div class="modal-header">
        <span class="close" id="closeModalRD">×</span>
        <h2>Create folder</h2>
      </div>
      <div class="modal-body">
        <div class="modal-body-part">
          <div class="modal-message">Enter new name:</div>
        </div>
        <div class="modal-body-part">
          <input id="newNameTextField" type="text" class="inputText">
        </div>
        <div class="modal-body-part">
          <button class="btn" onclick="renameDirectory()">Rename</button>
        </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>

  </body>
</html>
